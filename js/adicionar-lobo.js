const $ = document
const url = 'https://lobinhos.herokuapp.com'
const path = '/wolves/'

const salvar = $.querySelector("#myForm")

const inserirNoSite = (nome, idade, desc, img) =>{
  let novo_lobo = $.createElement("div")
  novo_lobo.innerHTML = `
    <figure><img class="loboSombra" src=${img} width=450 height=327.75></figure>
    <div>
        <h3>${nome}</h3>
        <sub>Idade: ${idade} anos</sub>
        <br>
        <p class="caption">
            ${desc}
        </p>
    </div>
    `
  list.appendChild(novo_lobo)
}

salvar.addEventListener("submit", (event) => {
  event.preventDefault()
  let nome = $.querySelector('#input-do-lobinho').value
  let anos = $.querySelector('#input-idade').value
  let foto = $.querySelector('#link-da-foto').value
  let texto = $.querySelector('#descricao').value

  if (validateWolves(nome,anos,foto,texto)){
    let fetchBody = {
      "wolf": {
        "name": nome,
        "age": parseInt(anos),
        "link_image": foto,
        "description": texto
      }
    }
    let fetchConfig = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(fetchBody)
    }
    fetch(url+path, fetchConfig)
      .then(resp => resp.json())
      .then(element => {
        inserirNoSite(nome,anos,foto,texto)
      })
      .catch(error => console.warn(error))
  } else {
    console.log('Dados não passaram pela validação')
  }
})

function validateWolves(nome, age, img, desc) {
  return (
    inRange(nome.length, 4, 60) &&
    inRange(age, 0, 100) &&
    img != '' &&
    inRange(desc.length, 10, 255)
  )
}

const inRange = (x, min, max) => (x - min) * (x - max) <= 0

/* function validateWolves(nome, age, img, desc) {
  return (
    inRange(nome.length, 4, 60) &&
    inRange(age, 0, 100) &&
    img != '' &&
    inRange(desc.length, 10, 255)
  )
} */

/* function createWolves(nome, age, img, desc) {
  if (validateWolves(nome, age, img, desc) == true) {
    let fetchBody = {
      wolf: {
        name: nome,
        age: age,
        link_image: img,
        description: desc
      }
    }
    let fetchConfig = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(fetchBody)
    }
    fetch(url + path, fetchConfig)
      .then(resp => resp.json())
      .catch(error => console.warn(error))
  } else {
    console.log('Dados não passaram pela validação')
  }
}

salvar.addEventListener('click', function () {
  let lobo = {
    name: nome.value,
    age: anos.value,
    link_image: foto.value,
    description: texto.value
  }
  dados = [nome, anos, foto, texto]
  dados.forEach(dado => {
    dado.value = ''
  })
  createWolves(nome.value, anos.value, foto.value, texto.value)
}) */
